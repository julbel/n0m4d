
with SynthDef("warpy") as warpy:
  warpy.freq = warpy.freq / 2
  warpy.amp  = warpy.amp * 2
  warpy.freq = warpy.freq + [0,0.5]
  warpy.osc  = LPF.ar(Impulse.ar(warpy.freq), 3000)
  warpy.env  = Env.perc(sus=warpy.sus * 1.00)
  warpy.osc = RLPF.ar(warpy.osc,
    LFNoise1.kr(warpy.sus,warpy.freq*3,warpy.freq*3)
    +XLine.kr(30,warpy.freq*3, warpy.sus), 0.07, 10).distort*1.25

Scale.default = "chromatic"

b1 >> arpy([12,7,12,7,10,10,7,10,7], dur=[1,1,1,1,0.5,.5,1,1,1], oct=4)

b1.stop() #listen to the arpy version, then warpy

b1 >> warpy([12,7,12,7,10,10,7,10,7], dur=[1,1,1,1,0.5,.5,1,1,1], oct=4)


