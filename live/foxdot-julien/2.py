#p1 >> pluck().stop()
#print(SynthDefs)

# d1 >> sawbass().stop()
# p1 >>([0, 2, 4], dur=[1, 1/2, 1/2], amp=0.75)
#p1 >> pluck([0, 1, 2, 3], dur=2) + [0, 0, 4]
p2 >> pluck([0, 7, 2, 3], dur=6) + (0, 2, 4)
d9.stop()
# d1.stop()
d1 >> play("x-o--uv(------)r(fm)mmGHt(::::)")
d2 >> play("x---o-u-l-q-", dur=2)
# d1 >> play("x-o{-=*}bd:*")
d9 >> play("beat_dur")

# d1 >> play("(x[--])xo{-[--][-x]}")
#d1 >> play("(x-)(-x)o-")
# d1 >> play("x-o-")
# Rest every 3rd note for 2 beats

# p1 >> pluck([0, 1, 2, 3], dur=[1, 1, rest(2)])
# Change to minor
# Start a player in the default scale (Major)
p1 >> pluck([0, 2, 4, 6, 7])
 
# Change the default scale to Dorian
Scale.default = Scale.dorian
 
# You can specify the default scale as a string
Scale.default = "dorian"
p1 >> pluck([0, 2, 4, 6, 7], scale=Scale.minor)

p2 >> pluck(dur=4, slide=1, slidedelay=0.5)

p3 >> pluck(dur=PDur(3,8), blur=[1, 2])

p4 >> pluck([0, 2, 4, 7], dur=1/4, pan=linvar([-1,1],8))

p5 >> pluck(fmod=linvar([-10,10],8), dur=1/4, sus=1)

# Delay the slide effect to start half way through the note
p6 >> sawbass(dur=4, slide=0.5, slidedelay=0.5)
# Delay the bend effect to start half way through the note
p7 >> basestring(dur=1, slide=5, bend=0.1)
# Set the cutoff to change over time using a linvar
d1 >> play("x-o-", hpf=linvar([0,2000],32))
# Or reduce the sample rate for a different style of distortion!
d1 >> play("x:x:x:x:x:x:x:::x:x:x:x:x:x ", crush=32, bits=8)


d1 >> play("x * ", shape=0.5)
d1.stop()

p1 >> karp([0,5], shape=0.5, dur=8) + (0,4)
print(Samples)

d2 >> play("")
# Loop through the different levels we can apply the filter
p1 >> pluck(formant=P[:8])
p1.stop()

# Spreading a sound across stereo channels out of phase
d1 >> play("x-o-", pan=(-1, 1), pshift=(0, 0.125))
 
# This can be done using the "spread" method
d1 >> play("x-o-").spread()
 
# Instructions are interpreted left-to-right so spread overrides the "pan=[-1, 0, 1]"
d1 >> play("x-o-", pan=[-1, 0, 1]).spread()
 
# Using "spread" *before* the >> will set panning to [-1, 0, 1] but pshift will still be (0, 0.125)
d1.spread() >> play("x-o-", pan=[-1, 0, 1])

p1 >> pluck([0, 1, 2, 3, 4, 5, 6, 7]).every(8, "reverse")
d1 >> play("x-o-").every(6, "stutter", cycle=8)

# Play the event 4 times every 6 beats across 1/2 a beat
d3 >> play("x-o-", dur=1/2).every(6, "stutter", 4)
 
# Play the event 4 times every 6 beats across 3 beats
d2 >> play("x-o-", dur=1/2).every(6, "stutter", 4, dur=3)
 
# You can also specify the number of events to stutter using the 'n' keyword
d1 >> play("x-o-", dur=1/2).every(6, "stutter", dur=3, n=4)
 
# Trim the octave pattern to 3 every 4 beats
p1 >> pluck([0,1,2,3], oct=[4,5,6,7]).every(4, "oct.trim", 3)
p1.stop()

p1 >> pasha([0, 4], dur=[3/4, 3/4, 1/2]).every(3, "offadd", 2)

# Which is equivalent to:
    
p9 >> pluck([chromatic[0], chromatic[2], chromatic[4], scale[5], chromatic[11])
    
Clock.bpm = 55

Scale.default.set("minor")

Root.default.set(5)

u1 >> play("hihi",sample=var([4,7],3),dur=P[1/2,1,1/2],room=0.3)

po >> klank(P[1,5,3],dur=2,formant=var([0.02,0.05],8))

lu >> karp(dur=PDur(1,8), pan=PShuf(2)).follow(po)

lu.every(4,"stutter",4,lpf=1000,oct=8, scale=Scale.major, root=6)

pi >> bass(var([0,-2,-4,1],8),dur=1,formant=0.05)

lk >> dirt(dur=PDur(5,8),oct=5, drive=0.05,lpf=1000).follow(lu)

lu.never("stutter")

Clock.bpm = linvar([120,140],8)
Clock.bpm = 120
Scale.default.set("locrian")

lu.amplify=0

oi >> orient(P[0,5,6,8,7],dur=4,amp=[0,1],oct=4) + P(0,3,5)

ol >> play("#",dur=8, sample=4,room=0.5)

oi.stop()

li >> karp(dur=PDur(2,3)).follow(lk)

Scale.default.set("mixolydian")

ol.stop()

li.solo()

li.dur=PDur(1,2)

Clock.bpm=110
